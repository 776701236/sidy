import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-hebdomadaire',
  templateUrl: './hebdomadaire.page.html',
  styleUrls: ['./hebdomadaire.page.scss'],
})
export class HebdomadairePage implements OnInit {

  constructor(private menu: MenuController) { }

  ngOnInit() {
  }

  openMenu() {
    this.menu.open();

}
}
