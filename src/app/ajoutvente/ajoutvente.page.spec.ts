import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutventePage } from './ajoutvente.page';

describe('AjoutventePage', () => {
  let component: AjoutventePage;
  let fixture: ComponentFixture<AjoutventePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjoutventePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutventePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
