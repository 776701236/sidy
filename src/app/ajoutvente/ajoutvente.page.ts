import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PublicService } from '../services/public.service';

@Component({
  selector: 'app-ajoutvente',
  templateUrl: './ajoutvente.page.html',
  styleUrls: ['./ajoutvente.page.scss'],
})
export class AjoutventePage implements OnInit {

  constructor(private route:Router, public  publicService: PublicService ) { }

  ngOnInit() {
  }

 onAjout(vente) {
     this.publicService.ajoutvente({
       Designation: vente.Designation,
       price:vente.price,
     })
     .then(data =>{
       console.log(data);
    }, err => {
       console.log(err);
    });
    console.log(vente);
   
 }

}
