import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AprosposPage } from './aprospos.page';

describe('AprosposPage', () => {
  let component: AprosposPage;
  let fixture: ComponentFixture<AprosposPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AprosposPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AprosposPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
