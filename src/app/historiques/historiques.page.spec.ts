import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoriquesPage } from './historiques.page';

describe('HistoriquesPage', () => {
  let component: HistoriquesPage;
  let fixture: ComponentFixture<HistoriquesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoriquesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoriquesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
