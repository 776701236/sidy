import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import {rootUrl} from "../../app/apiurls/serverurls.js";

@Injectable({
  providedIn: 'root'
})
export class PublicService {

  constructor(private http:HTTP) { }





  ajoutvente(vente){
    return this.http.post(rootUrl+'/api/vente/1/', vente, {});
  }

  ajoutdepense(depense){
    return this.http.post(rootUrl+'/api/depense/1/', depense, {});
  }

}
