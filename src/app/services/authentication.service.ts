import { Injectable } from '@angular/core';
import {rootUrl} from "../../app/apiurls/serverurls.js";
import {Http, Headers} from '@angular/http';
import { HTTP } from '@ionic-native/http/ngx';


@Injectable()
export class AuthenticateService {


  public token: any;
 

  constructor(private http:HTTP) {

   }

   createUser(user){
    return this.http.post(rootUrl+'/api/registeruser/', user, {});
  }

   login(user){
    return this.http.post(rootUrl+'/api/loginuser/', user, {});
  }


 
  logout() {
    localStorage.removeItem('userToken');
  }

 }




