import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutentrePage } from './ajoutentre.page';

describe('AjoutentrePage', () => {
  let component: AjoutentrePage;
  let fixture: ComponentFixture<AjoutentrePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjoutentrePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutentrePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
