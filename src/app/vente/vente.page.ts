import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vente',
  templateUrl: './vente.page.html',
  styleUrls: ['./vente.page.scss'],
})
export class VentePage implements OnInit {

  constructor() { }

  ngOnInit() {
  }


  public vente = [
    {titre : "riz", prix:"3000"},
    {titre : "gaz", prix:"4500"},
    {titre : "vinagre", prix:"350"},
    {titre : "huile", prix:"500"},
    {titre : "lait", prix:"500"},
  ]
 
}
